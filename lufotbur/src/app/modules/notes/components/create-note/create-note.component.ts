import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { selectError } from 'src/app/modules/login/store/selectors/login-selectors';

@Component({
  selector: 'app-create-note',
  templateUrl: './create-note.component.html',
  styleUrls: ['./create-note.component.scss'],
})
export class CreateNoteComponent implements OnInit {
  noteForm!: FormGroup;
  errorMessage$!: Observable<string>;
  errorMessages = {
    title: {
      required: 'Title is required',
    },
    description: {
      required: 'description is required',
    },
  };
  constructor(private formBuilder: FormBuilder, private store: Store) {}
  ngOnInit(): void {
    this.initNoteForm();
    this.errorMessage$ = this.store.select(selectError);
  }
  initNoteForm(): void {
    this.noteForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      description: ['', [Validators.required]],
    });
  }
  /* Return AbstractControl To Form Control */
  formControlData(formControl: any): FormControl {
    return this.noteForm.get(formControl) as FormControl;
  }
  Note(): void {
    if (this.noteForm.invalid) {
      this.noteForm.markAllAsTouched();
      return;
    }
    // this.store.dispatch(LoginActions.logIn({ payload: this.noteForm.value }));
  }
}
