import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { catchError, exhaustMap, map, of, tap, withLatestFrom } from 'rxjs';
import { HttpService } from 'src/app/core/services/http/http.service';
import { Note, NotesGetResponse } from '../../interfaces/notes';
import { NotesActions } from '../actions/notes-type';
import { selectNoteListNext } from '../selectors/notes-selectors';

@Injectable()
export class NoteListEffects {
  NoteListEffect$ = createEffect(() =>
    this.actions$.pipe(
      ofType(NotesActions.CreateNote),
      exhaustMap((payload) =>
        this.httpService
          .post<Note>('/api/note/', payload.payload)
          .pipe(catchError((error) => of({ error })))
      ),
      tap((res) => {
        if (!('error' in res)) this.router.navigate(['notes']);
      }),
      map((res: any) => {
        if (res && 'error' in res) {
          return NotesActions.NoteListFail({
            error: res.error,
            errorMessage: 'somthing wrong',
          });
        }
        return NotesActions.NoteCreateed({ response: res });
      })
    )
  );

  NotesEffect$ = createEffect(() =>
    this.actions$.pipe(
      ofType(NotesActions.loadNoteList),
      exhaustMap((state) => {
        return this.httpService
          .get<NotesGetResponse>('/api/note/')
          .pipe(catchError((error) => of({ error })));
      }),
      map((res) => {
        if (res && 'error' in res) {
          let errorMessage = 'Unknown Error ';

          return NotesActions.NoteListFail({
            error: res.error,
            errorMessage: errorMessage,
          });
        }
        return NotesActions.NoteListLoaded({ NoteList: res });
      })
    )
  );

  NoteNextEffect$ = createEffect(() =>
    this.actions$.pipe(
      ofType(NotesActions.loadNoteListNext),
      withLatestFrom(this.store.select(selectNoteListNext)),
      exhaustMap(([type, next]) => {
        return !next
          ? of(null)
          : this.httpService
              .getNext<NotesGetResponse>(next)
              .pipe(catchError((error) => of({ error })));
      }),
      map((res) => {
        if (res && 'error' in res) {
          let errorMessage = 'Unknown Error ';
          return NotesActions.NoteListFail({
            error: res.error,
            errorMessage: errorMessage,
          });
        }
        return NotesActions.NoteListLoaded({ NoteList: res });
      })
    )
  );
  deleteNoteEffects$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(NotesActions.deleteNote),
        exhaustMap((action) => {
          return this.httpService
            .deleteReq<Note>(`api/note/${action.id}/`)
            .pipe(
              map((res) => of({ id: action.id })),
              catchError((error) => of({ error }))
            );
        })
      ),
    map((res) => {
      if (res && 'error' in res) {
        let errorMessage = 'Unknown Error ';

        return NotesActions.NoteListFail({
          error: res.error,
          errorMessage: errorMessage,
        });
      }
      return NotesActions.NoteListLoaded({ id: res });
    })
  );
  constructor(
    private actions$: Actions,
    private httpService: HttpService,
    private store: Store,
    private router: Router
  ) {}
}
