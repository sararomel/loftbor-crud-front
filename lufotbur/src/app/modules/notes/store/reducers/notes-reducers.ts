import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { Note } from '../../interfaces/notes';
import { NotesActions } from '../actions/notes-type';

export interface Notestate extends EntityState<Note> {
  count: number;
  next: string | null;
  previous: string | null;
  loaded: boolean;
  errorMessage: string;
}
export const NotesAdapter: EntityAdapter<Note> = createEntityAdapter<Note>();
export const initState: Notestate = NotesAdapter.getInitialState({
  count: 0,
  next: null,
  previous: null,
  loaded: false,
  errorMessage: '',
});
export const NotesReducer = createReducer(
  initState,
  on(NotesActions.upsertNote, (state, action) =>
    NotesAdapter.upsertOne(action.Note, state)
  ),
  on(NotesActions.NoteDeleted, (state, action) =>
    NotesAdapter.removeOne(action.id, state)
  ),
  on(NotesActions.upsertNotes, (state, action) =>
    NotesAdapter.upsertMany(action.Notes.results, {
      ...state,
      next: action.Notes.next,
      previous: action.Notes.previous,
    })
  ),
  on(NotesActions.NoteListLoaded, (state, action) =>
    action.NoteList
      ? NotesAdapter.upsertMany(action.NoteList.results, {
          ...state,
          loaded: true,
          errorMessage: '',
          next: action.NoteList.next,
          previous: action.NoteList.previous,
        })
      : state
  ),
  on(NotesActions.NoteCreateed, (state, action) =>
    NotesAdapter.upsertOne({ ...action.response }, state)
  ),
  on(NotesActions.NoteUpdateed, (state, action) =>
    NotesAdapter.upsertOne({ ...action.response }, state)
  ),

  on(NotesActions.NoteListFail, (state, action) => ({
    ...state,
    loaded: false,
    errorMessage: action.errorMessage,
  }))
);
// get the selectors
export const { selectIds, selectEntities, selectAll, selectTotal } =
  NotesAdapter.getSelectors();
