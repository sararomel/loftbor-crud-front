import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { Note, NotePayload, NotesGetResponse } from '../../interfaces/notes';

export const upsertNote = createAction(
  '[Notes effect ] Upsert Note',
  props<{ Note: Note }>()
);
export const upsertNotes = createAction(
  '[Notes effect ] Upsert Notes',
  props<{ Notes: NotesGetResponse }>()
);
export const loadNoteList = createAction('[NoteList effect ] load Note list');
export const NoteListLoaded = createAction(
  '[NoteList effect ] Note list  Loaded',
  props<{ NoteList: NotesGetResponse | null }>()
);
export const loadNoteListNext = createAction(
  '[NoteList effect ] load Note  list next '
);

export const CreateNote = createAction(
  '[ NoteList effect] Note Create ',
  props<{ payload: NotePayload }>()
);
export const NoteCreateed = createAction(
  '[ Effect] Note Created ',
  props<{ response: Note }>()
);
export const UpdateNote = createAction(
  '[ NoteList effect] Note Update ',
  props<{ payload: NotePayload }>()
);
export const NoteUpdateed = createAction(
  '[ NoteList effect] Note Updateed ',
  props<{ response: Note }>()
);
export const deleteNote = createAction(
  '[NoteList effect] Note delete',
  props<{ id: number }>()
);
export const NoteDeleted = createAction(
  '[NoteList effect] NoteDeleted',
  props<{ id: number }>()
);
export const NoteListFail = createAction(
  '[NoteList effect] Note list  fail',
  props<{ error: HttpErrorResponse; errorMessage: string }>()
);
