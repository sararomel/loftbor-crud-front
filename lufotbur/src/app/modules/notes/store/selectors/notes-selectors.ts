import { createFeatureSelector, createSelector } from '@ngrx/store';
import { Notestate, selectEntities } from '../reducers/notes-reducers';
// select the dictionary of user entities
export const selectNotesState = createFeatureSelector<Notestate>('Notes');
export const selectNotesEntities = createSelector(
  selectNotesState,
  selectEntities
);
export const selectNoteListNext = createSelector(
  selectNotesState,
  (noteListState): string | null => noteListState.next
);
