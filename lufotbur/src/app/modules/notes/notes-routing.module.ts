import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateNoteComponent } from './components/create-note/create-note.component';
import { NotesComponent } from './pages/notes/notes.component';

const routes: Routes = [
  {
    path: '',
    component: NotesComponent,
    title: 'Notes',
  },
  {
    path: 'create-note',
    component: CreateNoteComponent,
    title: 'Notes',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NotesRoutingModule {}
