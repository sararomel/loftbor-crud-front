import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SharedModule } from 'src/app/shared/shared.module';
import { CreateNoteComponent } from './components/create-note/create-note.component';
import { NotesItemComponent } from './components/notes/notes-item.component';
import { NotesRoutingModule } from './notes-routing.module';
import { NotesComponent } from './pages/notes/notes.component';

@NgModule({
  declarations: [NotesComponent, NotesItemComponent, CreateNoteComponent],
  imports: [CommonModule, NotesRoutingModule, SharedModule],
})
export class NotesModule {}
